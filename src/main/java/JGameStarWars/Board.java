package JGameStarWars;

import JGameStarWars.models.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;
import java.util.List;
import javax.imageio.ImageIO;
public class Board extends JPanel implements ActionListener {
    private Timer timer;
    private SpaceShip spaceShip;
    private List<Missile> missileList;
    private List<Asteroid> asteroidList;
    private List<Explosion> explosionList;
    private boolean inGame;
    private BufferedImage background;

    private int widthScreen;
    private int heightScreen;
    public Board() {
        initBoard();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);
        if (inGame) {
            drawObjects(g);
        } else {
            if (asteroidList.size() == 0) {
                drawMessage(g, "Victory!");
            } else {
                drawMessage(g, "Game over");
            }
        }
        Toolkit.getDefaultToolkit().sync();
    }

    private void drawMessage(Graphics g, String msg) {
        String messageExit = "Press <Esc> to exit or any key to repeat";
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics fontMetrics = getFontMetrics(font);
        g.setColor(Color.WHITE);
        g.setFont(font);
        g.drawString(msg,
                (widthScreen - fontMetrics.stringWidth(msg)) / 2,
                heightScreen / 3);
        g.drawString(messageExit,
                (widthScreen - fontMetrics.stringWidth(messageExit)) / 2,
                heightScreen / 2 );
    }

    private void drawObjects(Graphics g) {
        drawObject(g, spaceShip);
        for (Missile missile : missileList) {
            drawObject(g, missile);
        }
        for (Asteroid asteroid : asteroidList) {
            drawObject(g, asteroid);
        }
        for (Explosion explosion : explosionList) {
            drawObject(g, explosion);
        }
        g.setColor(Color.WHITE);
        g.drawString("Asteroids left: " + asteroidList.size(), 5, 15);
    }

    private void drawObject(Graphics g, Sprite sprite) {
        if (sprite.isVisible()) {
            g.drawImage(sprite.getImage(),
                    sprite.getX(),
                    sprite.getY(),
                    this);
        }
    }
    private void initBoard() {
        addKeyListener(new TAdapter());
        setFocusable(true);
        try (InputStream backgroundImg = getClass().getResourceAsStream("/images/background.png")) {
            background = ImageIO.read(Objects.requireNonNull(backgroundImg));
            widthScreen = background.getWidth();
            heightScreen = background.getHeight();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            System.err.println("Background image missing");
            System.exit(1);
        }
        setPreferredSize(new Dimension(background.getWidth(), background.getHeight()));
        missileList = new ArrayList<>();
        initSpaceShip();
        initAsteroids();
        initExplosion();

        inGame = true;

        final int DELAY = 15;
        timer = new Timer(DELAY, this);
        timer.start();
    }

    private void initSpaceShip() {
        spaceShip = new SpaceShip((widthScreen / 4) - 100, (heightScreen / 2) - 50, missileList);
    }

    private void initExplosion() {
        explosionList = new ArrayList<>();
    }

    private void initAsteroids() {
        asteroidList = new ArrayList<>();
        final int COUNT_ASTEROIDS = 30;
        for (int i = 0; i < COUNT_ASTEROIDS; i++) {
            // Генерируем астероиды за пределами экрана
            int x = (int) (widthScreen + Math.random() * widthScreen);
            int y = (int) (Math.random() * (heightScreen - 50));
            asteroidList.add(new Asteroid(x, y));
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        updateMissiles(this);
        updateAsteroids(this);
        updateExplosion();
        updateShip(this);
        checkCollision();
        repaint();
        isInGame();
    }

    private void updateExplosion() {
        if (!explosionList.isEmpty()) {
            for (int i = 0; i < explosionList.size(); i++) {
                Explosion explosion = explosionList.get(i);
                if (explosion.isVisible()) {
                    explosion.modifyTimeLife();
                } else {
                    explosionList.remove(i--);
                }
            }
        }
    }

    private void checkCollision() {
        Rectangle spaceShipRectangle = spaceShip.getBounds();
        for (Asteroid asteroid : asteroidList) {
            Rectangle asteroidRectangle = asteroid.getBounds();
            if (spaceShipRectangle.intersects(asteroidRectangle)) {
                spaceShip.setVisible(false);
                inGame = false;
            }
        }
        for (Missile missile : missileList) {
            Rectangle missileRectangle = missile.getBounds();
            for (Asteroid asteroid : asteroidList) {
                Rectangle asteroidRectangle = asteroid.getBounds();
                if (missileRectangle.intersects(asteroidRectangle)) {
                    missile.setVisible(false);
                    asteroid.setVisible(false);
                    Explosion explosion = new Explosion(asteroid.getX(), asteroid.getY());
                    explosionList.add(explosion);
                    explosion.performSound();
                }
            }
        }
    }

    private void updateAsteroids(Board board) {
        if (asteroidList.isEmpty()) {
            inGame = false;
            return;
        }
        for (int i = 0; i < asteroidList.size(); i++) {
            Asteroid asteroid = asteroidList.get(i);
            if (asteroid.isVisible()) {
                asteroid.performMove(board);
                asteroid.performRotate();
            } else {
                asteroidList.remove(i--);
            }
        }
    }

    private void updateShip(Board board) {
        if (spaceShip.isVisible()) {
            spaceShip.performMove(board);
        }
    }

    private void updateMissiles(Board board) {
        for (int i = 0; i < missileList.size(); i++) {
            Missile missile = missileList.get(i);
            if (missile.isVisible()) {
                missile.performMove(board);
            } else {
                missileList.remove(i--);
            }
        }
    }

    public void isInGame() {
        if (!inGame) {
            timer.stop();
        }
    }

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
            spaceShip.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (!inGame) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ESCAPE) {
                    System.exit(0);
                }
                initBoard();
            } else {
                spaceShip.keyPressed(e);
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
            spaceShip.keyTyped(e);
        }
    }
}
