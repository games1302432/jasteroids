package JGameStarWars;

import javax.swing.*;
import java.awt.*;

public class JGameStarWars extends JFrame {
    public JGameStarWars() {
        initUI();
    }

    private void initUI() {
        add(new Board());
        setResizable(false);
        pack();
        setTitle("Star wars");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JGameStarWars gameStarWars = new JGameStarWars();
            gameStarWars.setVisible(true);
        });
    }
}
