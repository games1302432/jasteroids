package JGameStarWars.interfaces;

import JGameStarWars.Board;
import JGameStarWars.models.Sprite;

public class ControlledMove implements MoveBehavior {
    Sprite sprite;
    private int dX;
    private int dY;

    public ControlledMove(Sprite sprite) {
        this.sprite = sprite;
    }

    @Override
    public void setDx(int dX) {
        this.dX = dX;
    }

    @Override
    public void setDy(int dY) {
        this.dY = dY;
    }

    @Override
    public void move(Board board) {
        sprite.setX(Math.max(sprite.getX() + dX, 1));
        sprite.setY(Math.max(sprite.getY() + dY, 1));
        sprite.setX(Math.min(sprite.getX(), board.getWidth() - sprite.getWidth() - 1));
        sprite.setY(Math.min(sprite.getY(), board.getHeight() - sprite.getHeight() - 1));
    }
}
