package JGameStarWars.interfaces;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedInputStream;
import java.util.Objects;

public class ExplosionSoundMp3 implements SoundBehavior {

    private final Player mp3Player;
    private final Thread soundThread;

    public ExplosionSoundMp3() {
        BufferedInputStream buffer = new BufferedInputStream(
                Objects.requireNonNull(getClass()
                        .getClassLoader()
                        .getResourceAsStream("sounds/explosion.mp3")));
        try {
            mp3Player = new Player(buffer);
        } catch (JavaLayerException e) {
            throw new RuntimeException(e);
        }

        soundThread = new Thread(() -> {
            try {
                mp3Player.play();
            } catch (JavaLayerException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void sound() {
        if (!soundThread.isAlive()) {
            soundThread.start();
        }
    }

    @Override
    public void soundOff() {
    }
}
