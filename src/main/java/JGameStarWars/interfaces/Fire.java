package JGameStarWars.interfaces;

import JGameStarWars.models.Missile;
import JGameStarWars.models.SpaceShip;

public class Fire implements FireBehavior {

    @Override
    public void fire(SpaceShip spaceShip) {
        Missile missile = new Missile(spaceShip.getX() + spaceShip.getWidth(),
                spaceShip.getY() + spaceShip.getHeight() / 2);
        spaceShip.geMmissileList().add(missile);
        missile.performSound();
    }
}
