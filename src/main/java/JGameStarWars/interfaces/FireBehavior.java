package JGameStarWars.interfaces;

import JGameStarWars.models.SpaceShip;

public interface FireBehavior {
    void fire(SpaceShip spaceShip);
}
