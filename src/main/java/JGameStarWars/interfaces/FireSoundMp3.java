package JGameStarWars.interfaces;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedInputStream;
import java.util.Objects;

public class FireSoundMp3 implements SoundBehavior {

    private final Player mp3Player;
    private final Thread threadSound;

    public FireSoundMp3() {
        BufferedInputStream buffer = new BufferedInputStream(
                Objects.requireNonNull(getClass()
                        .getClassLoader()
                        .getResourceAsStream("sounds/fire.mp3")));
        try {
            mp3Player = new Player(buffer);
        } catch (JavaLayerException e) {
            throw new RuntimeException(e);
        }
        threadSound = new Thread(() -> {
            try {
                mp3Player.play();
            } catch (JavaLayerException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public void sound() {
        threadSound.start();
    }

    @Override
    public void soundOff() {

    }
}
