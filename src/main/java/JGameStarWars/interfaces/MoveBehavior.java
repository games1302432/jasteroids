package JGameStarWars.interfaces;

import JGameStarWars.Board;

public interface MoveBehavior {

    void move(Board board);

    void setDx(int i);

    void setDy(int i);
}
