package JGameStarWars.interfaces;

import JGameStarWars.models.Asteroid;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class RotateAsteroid implements RotatedBehavior {
    private final Asteroid asteroid;
    static final int COUNT_FRAME = 8;
    private static final Image[] arrayImage = new Image[COUNT_FRAME];
    private int phase;
    private int delay = 0;
    private final boolean rotated;

    static {
        for (int i = 0; i < arrayImage.length; i++) {
            arrayImage[i] = new ImageIcon(Objects.requireNonNull(RotateAsteroid.class
                    .getResource("/images/asteroid-rotated/asteroid-" + i + ".png")))
                    .getImage();
        }
    }

    public RotateAsteroid(Asteroid asteroid) {
        this.asteroid = asteroid;
        this.phase = (int) (Math.random() * arrayImage.length);
        rotated = Math.random() < 0.5;
    }

    @Override
    public void rotate() {
        final int DELAY_ROTATE = 10;
        if (rotated) {
            if (delay++ == DELAY_ROTATE) {
                delay = 0;
                if (phase++ == arrayImage.length - 1) {
                    phase = 0;
                }
                asteroid.setImage(arrayImage[phase]);
            }
        }
    }
}
