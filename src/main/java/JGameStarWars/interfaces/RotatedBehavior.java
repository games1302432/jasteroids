package JGameStarWars.interfaces;

public interface RotatedBehavior {
    void rotate();
}
