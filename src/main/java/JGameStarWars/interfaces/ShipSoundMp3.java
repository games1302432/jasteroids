package JGameStarWars.interfaces;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedInputStream;
import java.util.Objects;

public class ShipSoundMp3 implements SoundBehavior {

    private Thread threadSound = null;
    private Player mp3Player;

    public ShipSoundMp3() {
    }

    // Работаем в параллельных потоках для избежания "фризов" игры
    @Override
    public void sound() {
        if ((threadSound == null) || !threadSound.isAlive()) {
            BufferedInputStream buffer = new BufferedInputStream(
                    Objects.requireNonNull(getClass()
                            .getClassLoader()
                            .getResourceAsStream("sounds/ship.mp3")));
            threadSound = new Thread(() -> {
                try {
                    mp3Player = new Player(buffer);
                } catch (JavaLayerException e) {
                    throw new RuntimeException(e);
                }
                try {
                    mp3Player.play();
                } catch (JavaLayerException e) {
                    throw new RuntimeException(e);
                }
            });
            threadSound.start();
        }
    }

    @Override
    public void soundOff() {
        new Thread( () -> mp3Player.close()).start();
    }
}
