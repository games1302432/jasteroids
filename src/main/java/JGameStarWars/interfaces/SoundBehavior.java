package JGameStarWars.interfaces;

public interface SoundBehavior {
    void sound();

    void soundOff();
}
