package JGameStarWars.interfaces;

import JGameStarWars.Board;
import JGameStarWars.models.Asteroid;

public class UncontrolledMoveAsteroid implements MoveBehavior {
    Asteroid asteroid;
    final int D_MOVE_X;

    public UncontrolledMoveAsteroid(Asteroid asteroid) {
        this.asteroid = asteroid;
        D_MOVE_X = 1 + (int) (Math.random() * 3); // Случайная скорость для астероида [1;3]
    }

    @Override
    public void move(Board board) {
        asteroid.setX(asteroid.getX() - D_MOVE_X);
        if (asteroid.getX() < -asteroid.getWidth()) {
            asteroid.setX(board.getWidth() + asteroid.getWidth());
        }
    }


    @Override
    public void setDx(int i) {
    }

    @Override
    public void setDy(int i) {
    }
}
