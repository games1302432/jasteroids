package JGameStarWars.interfaces;

import JGameStarWars.Board;
import JGameStarWars.models.Sprite;

public class UncontrolledMoveMissile implements MoveBehavior {
    Sprite sprite;
    final int D_MOVE_X = 2;

    public UncontrolledMoveMissile(Sprite sprite) {
        this.sprite = sprite;
    }
    @Override
    public void move(Board board) {
        sprite.setX(sprite.getX() + D_MOVE_X);
        if (sprite.getX() > board.getWidth()) {
            sprite.setVisible(false);
        }
    }

    @Override
    public void setDx(int i) {
    }

    @Override
    public void setDy(int i) {
    }
}
