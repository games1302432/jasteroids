package JGameStarWars.models;

import JGameStarWars.interfaces.UncontrolledMoveAsteroid;
import JGameStarWars.interfaces.RotateAsteroid;
public class Asteroid extends Sprite {
    public Asteroid(int x, int y) {
        super(x, y, "/images/asteroid-rotated/asteroid-0.png");
        setMoveBehavior(new UncontrolledMoveAsteroid(this));
        setRotatedBehavior(new RotateAsteroid(this));
    }
}
