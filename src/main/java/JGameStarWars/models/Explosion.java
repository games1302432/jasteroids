package JGameStarWars.models;

import JGameStarWars.interfaces.ExplosionSoundMp3;

public class Explosion extends Sprite {
    final int TIME_LIFE = 7;
    private int timeLife;

    public Explosion(int x, int y) {
        super(x, y, "/images/explosion.png");
        timeLife = TIME_LIFE;
        setSoundBehavior(new ExplosionSoundMp3());
    }

    public void modifyTimeLife() {
        if (--timeLife == 0) {
            setVisible(false);
        }
    }
}
