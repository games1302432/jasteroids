package JGameStarWars.models;

import JGameStarWars.interfaces.FireSoundMp3;
import JGameStarWars.interfaces.UncontrolledMoveMissile;

public class Missile extends Sprite {
    public Missile(int x, int y) {
        super(x, y, "/images/missile.png");
        setMoveBehavior(new UncontrolledMoveMissile(this));
        setSoundBehavior(new FireSoundMp3());
    }
}
