package JGameStarWars.models;

import JGameStarWars.interfaces.ControlledMove;
import JGameStarWars.interfaces.Fire;
import JGameStarWars.interfaces.ShipSoundMp3;

import java.awt.event.KeyEvent;
import java.util.List;

public class SpaceShip extends Sprite {
    private final List<Missile> missileList;

    public List<Missile> geMmissileList() {
        return missileList;
    }
    public SpaceShip(int x, int y, List<Missile> missileList) {
        super(x, y, "/images/spaceship.png");
        setMoveBehavior(new ControlledMove(this));
        setFireBehavior(new Fire());
        this.missileList = missileList;
        setSoundBehavior(new ShipSoundMp3());
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            moveBehavior.setDx(-1);
            performSound();
        }
        if (key == KeyEvent.VK_RIGHT) {
            moveBehavior.setDx(1);
            performSound();
        }
        if (key == KeyEvent.VK_UP) {
            moveBehavior.setDy(-1);
            performSound();
        }
        if (key == KeyEvent.VK_DOWN) {
            moveBehavior.setDy(1);
            performSound();
        }
    }

    public void keyTyped(KeyEvent e) {
        int key = e.getKeyChar();
        if (key == KeyEvent.VK_SPACE) {
            performFire();
        }
        if (key == KeyEvent.VK_ESCAPE)
            System.exit(0);
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            moveBehavior.setDx(0);
            performSoundOff();
        }
        if (key == KeyEvent.VK_RIGHT) {
            moveBehavior.setDx(0);
            performSoundOff();
        }
        if (key == KeyEvent.VK_UP) {
            moveBehavior.setDy(0);
            performSoundOff();
        }
        if (key == KeyEvent.VK_DOWN) {
            moveBehavior.setDy(0);
            performSoundOff();
        }
    }
}
