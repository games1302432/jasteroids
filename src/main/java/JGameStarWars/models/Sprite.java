package JGameStarWars.models;

import JGameStarWars.Board;
import JGameStarWars.interfaces.FireBehavior;
import JGameStarWars.interfaces.MoveBehavior;
import JGameStarWars.interfaces.RotatedBehavior;
import JGameStarWars.interfaces.SoundBehavior;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.Objects;

public abstract class Sprite {
    private int x;
    private int y;
    private int width;
    private int height;
    private boolean visible;
    private Image image;
    protected MoveBehavior moveBehavior;
    protected FireBehavior fireBehavior;
    protected SoundBehavior soundBehavior;


    private RotatedBehavior rotatedBehavior;

    public void setX(int x) {
        this.x = x;
    }
    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public int getY() {
        return y;
    }
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }
    public void setImage(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    public void setMoveBehavior(MoveBehavior moveBehavior) {
        this.moveBehavior = moveBehavior;
    }
    public void setFireBehavior(FireBehavior fireBehavior) {
        this.fireBehavior = fireBehavior;
    }

    public void setRotatedBehavior(RotatedBehavior rotatedBehavior) {
        this.rotatedBehavior = rotatedBehavior;
    }
    public void setSoundBehavior(SoundBehavior soundBehavior) {
        this.soundBehavior = soundBehavior;
    }

    public Sprite(int x, int y, String imageFileName) {
        this.x = x;
        this.y = y;
        loadImage(imageFileName);
        visible = true;
    }

    protected void loadImage(String imageFileName) {
        URL imgURL = getClass().getResource(imageFileName);
        ImageIcon imageIcon = new ImageIcon(Objects.requireNonNull(imgURL));
        image = imageIcon.getImage();
        setHeight(imageIcon.getIconHeight());
        setWidth(imageIcon.getIconWidth());
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public void performMove(Board board) {
        moveBehavior.move(board);
    }

    public void performFire() {
        fireBehavior.fire((SpaceShip) this);
    }

    public void performSound() {
        // Для избежания "фризов" воспроизведение звука запускаем в параллельном потоке
        //new Thread(() -> soundBehavior.sound()).start();
        soundBehavior.sound();
    }

    public void performSoundOff() {
        soundBehavior.soundOff();
    }

    public void performRotate() {
        rotatedBehavior.rotate();
    }
}
